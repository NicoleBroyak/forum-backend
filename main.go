package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak/forum-backend/db"
	"gitlab.com/nicolebroyak/forum-backend/logger"
)

func main() {
	dbConn := db.Connect()
	logger.L.Info(dbConn.Name())
	router := gin.Default()

	api := router.Group("/api")
	api.GET("/ping", pongHandler)

	router.Run() // listen and serve on 0.0.0.0:8080
}

func pongHandler(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}
