package db

import (
	"gitlab.com/nicolebroyak/forum-backend/logger"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func Connect() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("forum.db"), &gorm.Config{})
	if err != nil {
		logger.L.Fatal(err)
	}

	logger.L.Info("successfully connected to database")
	return db
}
