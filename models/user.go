package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Name string
}

// gorm.Model definition
// type Model struct {
// ID        uint           `gorm:"primaryKey"`
//  CreatedAt time.Time
//  UpdatedAt time.Time
//  DeletedAt gorm.DeletedAt `gorm:"index"`
// }
